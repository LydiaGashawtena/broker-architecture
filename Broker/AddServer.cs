﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class AddServer : IServer
    {
        public ServiceType Service { get; set; }

        public double ProvideService(params double[] list)
        {
            double sum = list[0];
            foreach (var num in list)
            {
                sum += num;
            }
            return sum;
        }
    }
}
