﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class BrokerClass
    {
        List<IServer> servers = new List<IServer>();

        public BrokerClass()
        {
            servers.Add(new AddServer());
            servers.Add(new MultiplyServer());
            servers.Add(new DivideServer());
        }

        private IServer findServer(String type)
        {

            IServer found = null;
            foreach (var server in servers)
            {
                if (server.Service.ToString().Equals(type))
                {
                    Console.WriteLine(type.ToString() + "       " + server.Service.ToString());
                    found = server;
                    Console.WriteLine(found.Service.ToString());
                    break;
                }
            }
            return found;


        }

        public double GetService(Request request)
        {
            Console.WriteLine(request.Service);
            IServer server = findServer(request.Service.ToString());
            if (server == null)
            {
                Console.WriteLine("In if");
                return 0.0;
            }
            else
            {
                return server.ProvideService(request.list);
            }

        }
    }
}
