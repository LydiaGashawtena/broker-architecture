﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Broker
{
    /// <summary>
    /// Interaction logic for Client.xaml
    /// </summary>
    public partial class Client : Window
    {
        List<Course> courses = new List<Course>();
        public Client()
        {
            InitializeComponent();
        }


        private void addCourse_clicked(object sender, RoutedEventArgs e)
        {
            Label title = new Label();
            title.Content = add_course_title.Text;
            title.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
            title_panel.Children.Add(title);
            Label ects = new Label();
            ects.Content = add_ects.Text;
            ects.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
            ects_panel.Children.Add(ects);
            Label grade = new Label();
            grade.Content = add_grade.Text;
            grade.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
            grade_panel.Children.Add(grade);

            Course course = new Course();
            course.title = add_course_title.Text;
            course.ects = int.Parse(add_ects.Text);
            course.grade = double.Parse(add_grade.Text);

            courses.Add(course);

            add_course_title.Text = "";
            add_ects.Text = "";
            add_grade.Text = "";

        }

        private void calculate_clicked(object sender, RoutedEventArgs e)
        {
            GpaCalculator calculator = new GpaCalculator();
            double output = calculator.calculate(courses);

            output_label.Content = output;

            courses = new List<Course>();
            title_panel.Children.Clear();
            ects_panel.Children.Clear();
            grade_panel.Children.Clear();
        }
    }
}
