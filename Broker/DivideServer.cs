﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class DivideServer : IServer
    {
        public ServiceType Service { get; set; }

        public double ProvideService(params double[] list)
        {
            double quotient = list[0];
            foreach (var num in list)
            {
                quotient /= num;
            }
            return quotient;
        }
    }
}
