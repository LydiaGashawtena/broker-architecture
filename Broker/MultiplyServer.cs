﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    class MultiplyServer : IServer
    {
        public ServiceType Service { get; set; }

        public double ProvideService(params double[] list)
        {
            double product = list[0];
            foreach (var num in list)
            {
                product *= list[1];
            }
            return product;
        }
    }
}
