﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class GpaCalculator
    {
        public double calculate(List<Course> courses)
        {
            BrokerClass broker = new BrokerClass();
            List<double> ectsXgrade = new List<double>();
            List<double> ectsList = new List<double>();
            foreach (var course in courses)
            {
                Request request = new Request();

                request.Service = ServiceType.MULTIPLY;
                double[] temp1 = { course.ects, course.grade };
                request.list = temp1;

                ectsXgrade.Add(broker.GetService(request));
                ectsList.Add(course.ects);
            }

            Request request2 = new Request();
            request2.Service = ServiceType.ADD;
            request2.list = ectsXgrade.ToArray();
            double sum = broker.GetService(request2);

            Request request3 = new Request();
            request3.Service = ServiceType.ADD;
            request3.list = ectsList.ToArray();
            double ectsSum = broker.GetService(request3);

            Request request4 = new Request();
            request4.Service = ServiceType.DIVIDE;
            double[] temp2 = { sum, ectsSum };
            request4.list = temp2;
            double gpa = broker.GetService(request4);

            return gpa;
        }
    }
}
