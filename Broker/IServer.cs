﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public enum ServiceType { ADD, MULTIPLY, DIVIDE }
    public interface IServer
    {
        ServiceType Service { get; set; }

        double ProvideService(params double[] list);
    }
}
