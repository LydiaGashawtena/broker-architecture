﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class Request
    {
        public ServiceType Service { get; set; }
        public double[] list { get; set; }
    }
}
