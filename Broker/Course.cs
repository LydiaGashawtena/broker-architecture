﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class Course
    {
        public string title { get; set; }
        public int ects { get; set; }
        public double grade { get; set; }
    }
}
